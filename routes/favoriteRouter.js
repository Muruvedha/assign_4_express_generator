const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var authenticate = require('../authenticate');
const cors = require('./cors');
const Favorites = require('../models/favorite');

const favoriteRouter = express.Router();

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, authenticate.verifyUser, (req,res,next) => {
    Favorites.findOne({ user : req.user.id})
    .populate('user')
    .populate('dishes')
    .then((favorite) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorite);
    }, (err) => next(err))
    .catch((err) => next(err));
})

.post(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
  Favorites.findOne({ user : req.user.id})
    .then((favorite) => {
 if (favorite== null) {  
        Favorites.create({"user": req.user.id, "dishes": req.body})
     
            .then((favorite) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);                 
            }, (err) => next(err));
        
        }
        else {

         
        for (var i=0 ; i<req.body.length; i++)
        {
                if(favorite.dishes.indexOf(req.body[i]._id) === -1)
                {
                     favorite.dishes.push(req.body[i]._id);
                }

            }
                favorite.save()                    
            .then((favorite) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json'); 
                    res.json(favorite);                 
            }, (err) => next(err));
        
	            
                
		} 
      
    }, (err) => next(err))
    .catch((err) => next(err));
})

.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /favorites');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({ user : req.user.id})
    .then((favorite)=> {
   favorite.remove()
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    }, (err) => next(err))
    .catch((err) => next(err));    
});

favoriteRouter.route('/:favoriteId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, authenticate.verifyUser, (req,res,next) => {
  res.statusCode = 403;
    res.end('GET operation not supported on /favorites/' + req.params.favoriteId);
})
.post(cors.corsWithOptions,authenticate.verifyUser,  (req, res, next) => {
   Favorites.findOne({ user : req.user.id})
    .then((favorite) => {
        if (favorite== null) {  
        req.body.user = req.user.id;
        req.body.dishes=req.params.favoriteId;
        Favorites.create(req.body)
        .then((favorite) => {
        Favorites.findOne({ user : req.user.id})
       
            .then((favorite) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);                 
            }, (err) => next(err));
        },(err) => next(err));
        }
        else 
{  
    for(var j = (favorite.dishes.length -1); j >= 0; j-- )
        {
            if(favorite.dishes.indexOf(req.params.favoriteId) === -1)
                {
                
                   favorite.dishes.push(req.params.favoriteId);
                   }
                   }
                   favorite.save() 
				   .then((favorite) => 
					{
						res.statusCode = 200;
						res.setHeader('Content-Type', 'application/json');
						res.json(favorite);                 
					 },(err) => next(err));
	            
            
		          
}   
    }, (err) => next(err))
    .catch((err) => next(err));
})

.put(cors.corsWithOptions,authenticate.verifyUser,  (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /favorites/' + req.params.favoriteId);
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
Favorites.findOne({ user : req.user.id})
.then((favorite) => {

    favorite.dishes.remove(req.params.favoriteId)
    favorite.save()
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = favoriteRouter;